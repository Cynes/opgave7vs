package sock;
import java.io.*;
import java.net.*;
public class TCPServer {
	
	/**
	 * @param args
	 */
	public static void main(String[] args)throws Exception {
		String clientSentence;
		String capitalizedSentence;
		ServerSocket welcomeSocket = new ServerSocket(6789);
		while (true) {
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());	
			System.out.println("connecting IP: " + connectionSocket.getInetAddress());
			System.out.println("via port: " + connectionSocket.getPort());
			System.out.println("keep alive: " + connectionSocket.getKeepAlive());
			//connectionSocket.setSoTimeout(8000);
			while(connectionSocket.isConnected()) {
			clientSentence = inFromClient.readLine();
			
			//server refuses connection if a message is received
//			if(clientSentence != null) {
//				connectionSocket.close();
//			}
			
			capitalizedSentence = clientSentence.toUpperCase() + '\n';
			outToClient.writeBytes(capitalizedSentence);
			}
		}
	}

}
